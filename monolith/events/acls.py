from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_picture_url(query):
    url = f"https://api.pexels.com/v1/search?query={query}"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    response = requests.get(url, headers=headers)

    api_dict = response.json()

    picture = {"picture_url": api_dict["photos"][0]["src"]["original"]}

    return picture


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    params = {
        "q": f"{city}, {state}.USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"

    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # Create the URL for the current weather API with the latitude

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url = "http://api.openweathermap.org/data/2.5/weather"

    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }

    except (KeyError, IndexError):
        return None

    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
